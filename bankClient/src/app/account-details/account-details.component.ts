import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Account } from '../account';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.css']
})
export class AccountDetailsComponent implements OnInit {

  accId: number;
  account: Account = new Account();

  constructor(private route: ActivatedRoute, private accountService: AccountService) { }

  ngOnInit(): void {
    this.accId = this.route.snapshot.params['accId'];
    this.account = new Account();
    this.accountService.getAccountById(this.accId).subscribe(data => {
      this.account = data;
    });
  }

}
