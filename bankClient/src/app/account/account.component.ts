import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Account } from '../account';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  accounts: Account[];
  constructor(private accountService: AccountService, private router: Router) { }

  ngOnInit() {
    this.getAccounts();
  }
  private getAccounts() {
    this.accountService.getAccountList().subscribe(data => {
      this.accounts = data;
    });
  }
  updateAccount(accId: number) {
    console.log(`-----------`)
    this.router.navigate(['update-account', accId]);
  }
  deleteAccount(accId: number) {
    this.accountService.deleteAccount(accId).subscribe(data => {
      this.getAccounts();
      console.log(data);
    })
  }
  accountDetails(accId: number) {
    this.router.navigate(['account-details', accId]);
  }
}

