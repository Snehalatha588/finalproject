import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account } from './account';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private baseURL = "http://localhost:8080/bankapp/account";

  constructor(private httpClient: HttpClient) { }

  getAccountList(): Observable<Account[]> {
    // let username = 'sneha'
    // let password = 'sneha123'
    // const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    // return this.httpClient.get<Account[]>(`${this.baseURL}`, { headers });
    return this.httpClient.get<Account[]>(`${this.baseURL}`);
  }

  createAccount(account: Account): Observable<Object> {
    // let username = 'sneha'
    // let password = 'sneha123'
    // const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    // return this.httpClient.post(`${this.baseURL}`, account, { headers });
    return this.httpClient.post(`${this.baseURL}`, account);
  }

  getAccountById(accId: number): Observable<Account> {
    // let username = 'sneha'
    // let password = 'sneha123'
    // const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    // return this.httpClient.get<Account>(`${this.baseURL}/${accId}`, { headers });
    return this.httpClient.get<Account>(`${this.baseURL}/${accId}`);
  }

  updateAccount(accId: number, account: Account): Observable<Object> {
    // let username = 'sneha'
    // let password = 'sneha123'
    // const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    // return this.httpClient.put(`${this.baseURL}/${accId}`, account, { headers });
    return this.httpClient.put(`${this.baseURL}/${accId}`, account);
  }

  deleteAccount(accId: number): Observable<Object> {
    // let username = 'sneha'
    // let password = 'sneha123'
    // const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    // return this.httpClient.delete(`${this.baseURL}/${accId}`, { headers });
    return this.httpClient.delete(`${this.baseURL}/${accId}`);
  }
}
