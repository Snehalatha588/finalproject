export class Account {
    accId: number;
    name: string;
    balance: number;
    accountCreationDate: Date;
    accountType: string;
    address: string;
    phone: string;
    email: string;
    password: string;
    dob: Date;
}
