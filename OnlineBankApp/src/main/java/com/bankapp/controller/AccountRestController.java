package com.bankapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bankapp.dao.AuthResponse;
import com.bankapp.dto.DepositAmount;
import com.bankapp.dto.TransferAmount;
import com.bankapp.dto.WithdrawAmount;
import com.bankapp.entities.Account;
import com.bankapp.entities.TransactionLog;
import com.bankapp.service.AccountService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AccountRestController {

	@Autowired
	private AccountService accountService;

	@GetMapping(produces = "application/json")
	@RequestMapping({ "/validateLogin" })
	public AuthResponse validateLogin() {
		return new AuthResponse("User successfully authenticated");
	}

	@GetMapping(path = "account", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Account>> allAccounts() {
		List<Account> accounts = accountService.getAllAcounts();
		return ResponseEntity.ok(accounts);
	}

	@GetMapping(path = "transactions", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TransactionLog>> allTransactions() {
		List<TransactionLog> transactions = accountService.getAllTransactions();
		return ResponseEntity.ok(transactions);
	}

	@GetMapping(path = "account/{accId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Account> getAccountById(@PathVariable(name = "accId") int accId) {
		Account account = accountService.getAccountById(accId);
		return new ResponseEntity<Account>(account, HttpStatus.OK);
	}

	@PostMapping(path = "account", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Account> addNewAccount(@RequestBody Account account) {
		Account addAccount = accountService.addAccount(account);
		return new ResponseEntity<Account>(addAccount, HttpStatus.CREATED);
	}

	@PutMapping(path = "account/{accId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Account> updateAccount(@PathVariable(name = "accId") int accId,
			@RequestBody Account account) {
		Account updateAccount = accountService.updateAccount(accId, account);
		return new ResponseEntity<Account>(updateAccount, HttpStatus.CREATED);
	}

	@DeleteMapping(path = "account/{accId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Account> deleteAccount(@PathVariable(name = "accId") int accId) {
		Account deleteAccount = accountService.deleteAccount(accId);
		return new ResponseEntity<Account>(deleteAccount, HttpStatus.OK);
	}

	@PostMapping(path = "acctransfer", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String transferFund(@RequestBody TransferAmount transAmt) {
		accountService.transfer(transAmt.getFromId(), transAmt.getToId(), transAmt.getAmount());
		return "fund is transferred";
	}

	@PostMapping(path = "accdeposit", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String depositFund(@RequestBody DepositAmount depositAmt) {
		accountService.deposit(depositAmt.getAccountId(), depositAmt.getAmount());
		return "amount deposited successfully";
	}

	@PostMapping(path = "accwithdraw", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String withdrawFund(@RequestBody WithdrawAmount withdrawAmt) {
		accountService.withdraw(withdrawAmt.getAccountId(), withdrawAmt.getAmount());
		return "amount withdrawl successfully";
	}
}
